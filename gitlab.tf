variable "GITLAB_ACCESS_TOKEN" {
  type = string
}

provider "gitlab" {
  token = var.GITLAB_ACCESS_TOKEN
}

resource "gitlab_project" "test1" {
  name             = "test1"
  description      = "My awesome codebase"
  visibility_level = "public"
}

resource "gitlab_deploy_key" "example" {
  project  = gitlab_project.test1.id
  title    = "Example deploy key"
  key      = file("~/.ssh/id_rsa_default.pub")
  can_push = true

  depends_on = [gitlab_project.test1]

  provisioner "local-exec" {
    command = "git remote add origin ${gitlab_project.test1.ssh_url_to_repo}"
  }

  provisioner "local-exec" {
    command = "git push origin master"
  }
}

